﻿using PasswordManager.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager.ViewModels
{
    class MainViewModel : BaseViewModel
    {
        private ObservableCollection<Record> _records;
        private Record _selectedRecord;

        public MainViewModel()
        {
            // dummy data
            Records = new ObservableCollection<Record>();
            Records.Add(new Record()
            {
                Name = "Student Service",
                Group = "Faculty",
                Location = "http://www.ftn.uns.ac.rs",
                Description = "Web student service",
                Username = "E293/2016",
                Password = "password"
            });
            Records.Add(new Record()
            {
                Name = "Facebook",
                Group = "Social networks",
                Location = "http://www.facebook.com",
                Description = "Facebook account",
                Username = "john.doe@test.com",
                Password = "password"
            });
            Records.Add(new Record()
            {
                Name = "Google",
                Group = "Social networks",
                Location = "http://www.google.com",
                Description = "Google account",
                Username = "john.doe@google.com",
                Password = "password"
            });
            Records.Add(new Record()
            {
                Name = "Bitbucket",
                Group = "Office",
                Location = "http://www.bitbucket.com",
                Description = "Bitbucket account",
                Username = "john.doe@test.com",
                Password = "password"
            });
        }

        public ObservableCollection<Record> Records
        {
            get
            {
                return _records;
            }
            set
            {
                this._records = value;
                NotifyPropertyChanged("Records");
            }
        }

        public Record SelectedRecord
        {
            get
            {
                return _selectedRecord;
            }
            set
            {
                this._selectedRecord = value;
                NotifyPropertyChanged("SelectedRecord");
            }
        }
    }
}
