﻿using PasswordManager.Models;
using PasswordManager.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PasswordManager.ViewModels
{
    class EditViewModel : BaseViewModel
    {
        private Record _record;

        public EditViewModel()
        {
            MainView mainView = (MainView)Application.Current.MainWindow;
            Record selectedRecord = ((MainViewModel)mainView.DataContext).SelectedRecord;
            // populate model with selected record data
            Record = new Record()
            {
                Group = selectedRecord.Group,
                Name = selectedRecord.Name,
                Location = selectedRecord.Location,
                Description = selectedRecord.Description,
                Username = selectedRecord.Username,
                Password = selectedRecord.Password
            };
        }

        public Record Record
        {
            get
            {
                return _record;
            }
            set
            {
                _record = value;
                NotifyPropertyChanged("Record");
            }
        }
    }
}
