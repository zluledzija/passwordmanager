﻿using PasswordManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager.ViewModels
{
    class AddViewModel : BaseViewModel
    {
        private Record _record;

        public AddViewModel()
        {
            Record = new Record();
        }

        public Record Record
        {
            get
            {
                return _record;
            }
            set
            {
                _record = value;
                NotifyPropertyChanged("Record");
            }
        }
    }
}
