﻿using PasswordManager.Models;
using PasswordManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PasswordManager.Views
{
    /// <summary>
    /// Interaction logic for EditView.xaml
    /// </summary>
    public partial class EditView : Window
    {
        public EditView()
        {
            InitializeComponent();
            this.DataContext = new EditViewModel();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Record updatedRecord = ((EditViewModel)this.DataContext).Record;

            MainView mainView = (MainView)Application.Current.MainWindow;
            Record selectedRecord = ((MainViewModel)mainView.DataContext).SelectedRecord;
            ObservableCollection<Record> records = ((MainViewModel)mainView.DataContext).Records;

            // check if updated name is unique
            if (!selectedRecord.Name.Equals(updatedRecord.Name))
            {
                bool exists = false;
                foreach (var record in records)
                {
                    if (updatedRecord.Equals(record))
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                {
                    MessageBox.Show("Please provide different name. Record with the same name already exists.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    NameTextBox.Focus();

                    return;
                }
            }

            // update records
            ObservableCollection<Record> updatedRecords = new ObservableCollection<Record>();
            foreach (var record in records)
            {
                if (selectedRecord.Equals(record))
                {
                    updatedRecords.Add(updatedRecord);
                }
                else
                {
                    updatedRecords.Add(record);
                }
            }
            ((MainViewModel)mainView.DataContext).Records = updatedRecords;

            this.Close();
        }
    }
}
