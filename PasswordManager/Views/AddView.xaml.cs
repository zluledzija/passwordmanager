﻿using PasswordManager.Models;
using PasswordManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PasswordManager.Views
{
    /// <summary>
    /// Interaction logic for AddView.xaml
    /// </summary>
    public partial class AddView : Window
    {
        public AddView()
        {
            InitializeComponent();
            this.DataContext = new AddViewModel();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Record newRecord = ((AddViewModel) this.DataContext).Record;

            MainView mainView = (MainView)Application.Current.MainWindow;
            ObservableCollection<Record> records = ((MainViewModel) mainView.DataContext).Records;

            bool exists = false;
            foreach (var record in records)
            {
                if (newRecord.Equals(record))
                {
                    exists = true;
                    break;
                }
            }

            if (exists)
            {
                MessageBox.Show("Please provide different name. Record with the same name already exists.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                NameTextBox.Focus();
            }
            else
            {
                records.Add(newRecord);
                this.Close();
            }
        }
    }
}
