﻿using PasswordManager.Models;
using PasswordManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PasswordManager.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            this.DataContext = new MainViewModel();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddView addView = new AddView();
            addView.Owner = this;
            addView.ShowDialog();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            EditView editView = new EditView();
            editView.Owner = this;
            editView.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete selected record?", "Delete Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Record selectedRecord = ((MainViewModel) DataContext).SelectedRecord;
                ObservableCollection<Record> records = ((MainViewModel)DataContext).Records;
                foreach(var record in records)
                {
                    if (selectedRecord.Equals(record))
                    {
                        records.Remove(record);
                        break;
                    }
                }
            }
        }

        private void ImportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(this, "Import functionality is not implemented.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ExportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(this, "Export functionality is not implemented.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want exit?", "Exit confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        private void SettingsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(this, "Settings functionality is not implemented.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(this, "About functionality is not implemented.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
