﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager.Models
{
    class Record : IDataErrorInfo
    {
        public Record() { }
        
        public string Group { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public string Description { get; set; }

        #region IDataErrorInfo Members
        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string message = string.Empty;

                switch(propertyName)
                {
                    case "Group":
                        if (Group != null && Group.Length > 32)
                            message = "Maximum allowed length is 32 characters";

                        break;
                    case "Name":
                        if (string.IsNullOrWhiteSpace(Name))
                            message = "Name is required";
                        else if (Name.Length > 32)
                            message = "Maximum allowed length is 32 characters";

                        break;
                    case "Location":
                        if (Location != null && Location.Length > 48)
                            message = "Maximum allowed length is 48 characters";

                        break;
                    case "Username":
                        if (string.IsNullOrWhiteSpace(Username))
                            message = "Username is required";
                        else if (Username.Length > 48)
                            message = "Maximum allowed length is 48 characters";

                        break;
                    case "Password":
                        if (string.IsNullOrWhiteSpace(Password))
                            message = "Password is required";
                        else if (Password.Length > 48)
                            message = "Maximum allowed length is 48 characters";

                        break;
                    case "Description":
                        if (Description != null && Description.Length > 64)
                            message = "Maximum allowed length is 64 characters";

                        break;
                    default:
                        break;
                }

                return message;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            var record = obj as Record;
            if (record == null)
            {
                return false;
            }

            return this.Name.Equals(record.Name);
        }

        public override int GetHashCode()
        {
            return Name == null ? 0 : Name.GetHashCode();
        }
    }
}
